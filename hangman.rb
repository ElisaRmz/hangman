class Hangman
  attr_accessor :lifes

  def initialize(lifes: 6)
    self.lifes = lifes
  end

  def run

    puts "Para jugar debes elegir una palabra y yo tengo que averiguarla"

    # El programa solicita al jugador un patrón de palabra o de frase ej: "_____"
    print "¿Cuántas letras tiene? " 
    words_number = gets
    puts "¡Empecemos!", "_ " * words_number.to_i

    # El programa elige una letra, una palabra o una frase.

    # El usuario ha de indicar la posición si es una letra. Si la letra no está se muestra la letra tachada y una vida menos


    while !lifes.zero? #ó haya adivinado la palabra

      puts "¿En qué posición está esta letra '#{guess_letter}'?" 
      print "Escribe '0' si no aparece en tu palabra: "
      position = gets.to_i

      lifes -= 1 if position == 0
        
      print "Te quedan #{lifes} vidas. "
      print "#{show_word}"
      

    end

    puts "Game over" #se podría decir quíen ha ganado

  end

  def guess_letter
    ("a".."z").to_a.sample
  end

  def show_word
    puts 'en desarrollo' #muestra las letras dichas fuera de la palabra si no las tiene y dentro si las tiene
  end

end

Hangman.new(lifes: 3).run